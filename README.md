# Security Module
The "Security" module provides all the necessary components to support user authentication and hiding/showing content
based on user roles for any [OAuth 2.0](http://oauth.net/2/) token based authentication. This module was specifically
designed to work seamlessly with [Microsoft's MVC 5 OWIN authentication](http://blogs.msdn.com/b/webdev/archive/2013/07/03/understanding-owin-forms-authentication-in-mvc-5.aspx).

## Installation
The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
commands to install this package:

```
bower install https://bitbucket.org/transcend/bower-security.git
```

Other options:

* Download the code at [http://code.tsstools.com/bower-security/get/master.zip](http://code.tsstools.com/bower-security/get/master.zip)
* View the repository at [http://code.tsstools.com/bower-security](http://code.tsstools.com/bower-security)
* If you have permissions, view the source code at [http://code.tsstools.com/web-common](http://code.tsstools.com/web-common/src/ee541a3bce850ae77f6d7948a00cc352b4f67b21/modules/security/?at=develop)

When you have pulled down the code and included the files (and dependency files), simply add the module as a
dependency to your application:

```
angular.module('myModule', ['transcend.security']);
```

## Configuration
You can configure the [Security Module](http://docs.tsstools.com/#/api/transcend.security) by using the standard $provider configuration method
or by overriding the "[securityConfig](http://docs.tsstools.com/#/api/transcend.security.securityConfig)" object. This object is passed into each component in the
module and will use any properties set on the object before deferring to the default settings/configuration.

```
angular.module('myApp').value('securityConfig', {
resource: {
    // Override the default URL for all resources in this module.
    url: 'http://locahost/MyWebApiApp'
  }
});
```

## Usage
By default, the [Security Module](http://docs.tsstools.com/#/api/transcend.security) is configured to work out of the box with an MVC5 WebAPI2
back-end - with individual accounts setup (using Bearer token). For additional information on setting up the backend
project to support this front-end control, see the "MVC5 and WebAPI Authentication" section of the
[Resources Page](http://docs.tsstools.com/#/development/resources).

## To Do
- Add additional components for registration, password change, and role management.
- Add persisting the logged in user token.

## Module Goals
- Keep the minified module under 10KB.