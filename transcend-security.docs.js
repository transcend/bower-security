/**
 * @ngdoc overview
 * @name transcend.security
 * @description
 # Security Module
 The "Security" module provides all the necessary components to support user authentication and hiding/showing content
 based on user roles for any [OAuth 2.0](http://oauth.net/2/) token based authentication. This module was specifically
 designed to work seamlessly with [Microsoft's MVC 5 OWIN authentication](http://blogs.msdn.com/b/webdev/archive/2013/07/03/understanding-owin-forms-authentication-in-mvc-5.aspx).

 ## Installation
 The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
 commands to install this package:

 ```
 bower install https://bitbucket.org/transcend/bower-security.git
 ```

 Other options:

 *   * Download the code at [http://code.tsstools.com/bower-security/get/master.zip](http://code.tsstools.com/bower-security/get/master.zip)
 *   * View the repository at [http://code.tsstools.com/bower-security](http://code.tsstools.com/bower-security)
 *   * If you have permissions, view the source code at [http://code.tsstools.com/web-common](http://code.tsstools.com/web-common/src/ee541a3bce850ae77f6d7948a00cc352b4f67b21/modules/security/?at=develop)

 When you have pulled down the code and included the files (and dependency files), simply add the module as a
 dependency to your application:

 ```
 angular.module('myModule', ['transcend.security']);
 ```

 ## Configuration
 You can configure the {@link transcend.security Security Module} by using the standard $provider configuration method
 or by overriding the "{@link transcend.security.securityConfig securityConfig}" object. This object is passed into each component in the
 module and will use any properties set on the object before deferring to the default settings/configuration.

 ```
 angular.module('myApp').value('securityConfig', {
   resource: {
     // Override the default URL for all resources in this module.
     url: 'http://locahost/MyWebApiApp'
   }
 });
 ```

 ## Usage
 By default, the {@link transcend.security Security Module} is configured to work out of the box with an MVC5 WebAPI2
 back-end - with individual accounts setup (using Bearer token). For additional information on setting up the backend
 project to support this front-end control, see the "MVC5 and WebAPI Authentication" section of the
 {@link development/resources Resources Page}.

 ## To Do
 - Add additional components for registration, password change, and role management.

 ## Module Goals
 - Keep the minified module under 10KB.
 */
/**
 * @ngdoc object
 * @name transcend.security.securityConfig
 *
 * @description
 * Provides a single configuration object that can be used to change the behaviour, options, urls, etc for any
 * components in the {@link transcend.security Security Module}. The "securityConfig" object is a
 * {@link http://docs.angularjs.org/api/auto/object/$provide#value module value} and can therefore be
 * overridden/configured by:
 *
 <pre>
 // Override the value of the 'securityConfig' object.
 angular.module('myApp').value('securityConfig', {
     tokenRequiredUrlPattern: '/api/',
     account: {
       url: 'http://localhost/MyWebApiApp'
     }
   });
 </pre>
 **/
/**
 * @ngdoc property
 * @name transcend.security.securityConfig#tokenRequiredUrlPattern
 * @propertyOf transcend.security.securityConfig
 *
 * @description
 * Pattern that dictates whether a URL from a given HTTP request should have the authentication token added to the
 * headers. The __default__ is __'/api/'__ because this is the default route prefix for any WebAPI project. This means that
 * any URL that has the string '/api/' in it, will get the "Authentication"
 <pre>
 // Override the value of the 'securityConfig' object.
 angular.module('myApp').value('securityConfig', {
         tokenRequiredUrlPattern: '/authOnly/'
       });

 // Matches authentication pattern:
 POST http://some.server.com/authOnly/some-endpoint
 // Will add the token to the header:
 Authorization:Bearer KPlug77bE-2t3QjPkf7AHk-KOpT5YUQgR0x6Pcgui-hW9XMlXT19wvm4L...
 // Does not match authentication pattern:
 POST http://some.server.com/anything/some-endpoint
 // Will not add the token to the header:
 </pre>
 */
/**
 * @ngdoc object
 * @name transcend.security.securityConfig#resource
 * @propertyOf transcend.security.securityConfig
 *
 * @description
 * Configuration object to configure common resource properties, such as the server URL and default parameters.
 * All resources in the "{@link transcend.security Security Module}" extend the resource config object first, and
 * then extend the resource's specific configuration. So you can set the default backend server URL for all
 * resources, by using this configuration, and define any other configurations that may vary as well.
 *
 * ## Configuration Properties
 *   * __url__ - a string that that represents the base url to the backend server.
 *   * __params__ - default URL parameters that will be passed to each service call.
 *
 <pre>
 angular.module('myApp').value('securityConfig', {
       // The "Account" resource's URL will be set
       // by the resource url configuration.
       resource: {
         url: 'http://localhost/MyWebApiApplication'
       },
       // The "Role" resource's URL will initially be set by
       // the resource url configuration, but
       // will be overridden by the "role" configuration.
       // So ultimately the "Role" resource URL will
       // be 'http://otherhost/MyOtherApplication'.
       role: {
         url: 'http://otherhost/MyOtherApplication'
       }
     });
 </pre>
 */
/**
   * @ngdoc service
   * @name transcend.security.$authenticate
   *
   * @description
   * Provides an interface for authenticating (singing in) a user as well as signing out a user. The $authenticate service
   * will handling the token based authentication as well as setting the appropriate roles on the
   * {@link $roleManager role manager}. The actual HTTP request will be made through the
   * {@link transcend.security.Account#authenticate Account's authenticate method}.
   *
   <pre>
   // Sign in (the service will handle the token and setting roles):
   $authenticate({ username: 'user', password: 'password' })
   .then(successCallback, failCallback);

   // Sing out (the service will handle removing the token and roles):
   $authenticate.signOut();
   </pre>
   *
   * @requires $log
   * @requires $rootScope
   * @requires $http
   * @requires transcend.core.$loading
   * @requires transcend.core.$notify
   * @requires transcend.security.$roleManager
   * @requires transcend.security.Account
   *
   * @param {Object|String} credentials The credentials to use to authenticate a user. If an object is passed the object
   * must have a 'userName' property and a 'password' property - otherwise if a string is passed than that raw string will
   * get passed to the authentication REST service.
   */
/**
           * @ngdoc method
           * @name transcend.security.$authenticate#isAuthenticated
           * @propertyOf transcend.security.$authenticate
           *
           * @description
           * Flag that states whether a user has been authenticated or not. If a user has not been authenticated than the
           * {@link transcend.security.$authenticate#account account} property will be empty (or whatever the default is
           * configured to).
           * @example
           <pre>
           expect($authenticate.isAuthenticated).toBeFalsy();
           </pre>
           */
/**
           * @ngdoc method
           * @name transcend.security.$authenticate#account
           * @propertyOf transcend.security.$authenticate
           *
           * @description
           * Object that holds the account information. This includes the token and user details like; user name, email,
           * first name, last name, etc.
           * @example
           <pre>
           expect($authenticate.account.userName).toEqual('rgreen');
           expect($authenticate.account.firstName).toEqual('Rusty');
           expect($authenticate.account.lastName).toEqual('Green');
           </pre>
           */
/**
           * @ngdoc method
           * @name transcend.security.$authenticate#signOut
           * @methodOf transcend.security.$authenticate
           *
           * @description
           * Signs an authenticated user out. This will reset the $authenticate service and the {@link $roleManager} service
           * back to their original states.
           * @example
           <pre>
           $authenticate.signOut();
           </pre>
           */
/**
           * @ngdoc method
           * @name transcend.security.$authenticate#loadLocalToken
           * @methodOf transcend.security.$authenticate
           *
           * @description
           * Loads the token from local storage if the token exists in local storage - this requires that the user has
           * opted to "remember me" and the browser supports local storage. If there is no token, or the token has
           * expired, then no action will take place.
           * @example
           <pre>
           $authenticate.loadLocalToken();
           </pre>
           */
/**
 * @ngdoc directive
 * @name transcend.security.directive:inlineUserSignIn
 *
 * @description
 * The 'inlineUserSignIn' directive provides a compacted and simplified interface to let users enter their credentials
 * and be authenticated. This sign in directive abstracts the security module details and allows you to simply include
 * this directive and not have to do anything else to to support authentication.
 *
 * @restrict EA
 * @element ANY
 * @scope
 *
 * @requires $log
 * @requires transcend.security.$authenticate
 * @requires transcend.security.securityConfig
 *
 * @param {Function=} onSuccess Function called when the user is successfully authenticated (signed in).
 * @param {Function=} onFail Function called when a user's credentials fail and they are not authenticated.
 * @example
 <example module="app">
 <file name="index.html">
 <div ng-controller="Ctrl">
 <nav class="navbar navbar-default" role="navigation">
   <div class="navbar-default nav-inline-user-sign-in pull-right">
     <inline-user-sign-in on-success="onSuccess()" on-fail="onFail()"></inline-user-sign-in>
   </div>
 </nav>

 Enter a user name of: <strong>rgreen</strong><br/>
 Enter a password of: <strong>abc123</strong>
 <hr/>
 <div ng-style="style">{{message}}</div>
 </div>
 </file>
 <file name="script.js">
 angular.module('app', ['transcend.security', 'ngMockE2E'])
 .run(function($roleManager, $authenticate){
  $authenticate.rememberMe = false;
  $roleManager.enabled = false;
 })

 .controller('Ctrl', function($scope, $authenticate) {
    $scope.onSuccess = function() {
      $scope.message = 'Congratulations! You have successfully signed in.';
      $scope.style = {color: 'green'};
    };

    $scope.onFail = function() {
      $scope.message = 'Sorry! Your credentials were not valid.';
      $scope.style = {color: 'red'};
    };
});
 </file>
 </file>
 <file name="mocks.js">
 angular.module('app')
 .run(function($httpBackend){
   // Mock our HTTP calls.
   $httpBackend.whenPOST('/token', 'grant_type=password&username=rgreen&password=abc123')
    .respond({"access_token":"test-token","token_type":"bearer","expires_in":7775999,"id":"b6aad2d8-2fdd-4b07-91f9-ada394f1ee95","userName":"rgreen","roles":"","email":"","firstName":"Rusty","lastName":"Green",".issued":"Mon, 17 Aug 2015 17:03:31 GMT",".expires":"Sun, 15 Nov 2015 17:03:31 GMT"});

   $httpBackend.whenPOST('/token', /^(?!.*grant_type=password&username=rgreen&password=abc123)/)
    .respond(400, {"error":"invalid_grant","error_description":"The user name ('siteadmin') or password is incorrect."});

   $httpBackend.whenPOST('/api/account/signout')
    .respond('');
 })
 </file>
 </example>
 */
/**
     * @ngdoc object
     * @name transcend.security.securityConfig#inlineUserSignIn
     * @propertyOf transcend.security.securityConfig
     *
     * @description
     * Configuration object for the {@link transcend.security.directive:inlineUserSignIn Inline User Sign In Directive}.
     * This object provides the capability to override the directive's default configuration.
     *
     * ## Configuration Properties
     *   * __credentials__ - an object that holds the 'userName' and 'password' properties for a user.
     *
     <pre>
     angular.module('myApp').value('securityConfig', {
       // This will auto-fill in the user name input box
       // with "Guest" as the user name.
       inlineUserSignIn: {
         credentials: {
           userName: 'Guest',
           password: ''
         }
       }
     });
     </pre>
     *
     * __Note,__ all methods on a resource can be called on the resource instance by prefixing the method name with a
     * '$'. This is standard {@link transcend.security.service:Account resource} behaviour.
     */
/**
 * @ngdoc service
 * @name transcend.security.service:Account
 *
 * @description
 * A {@link http://docs.angularjs.org/api/ngResource/service/$resource $resource} to interact with an "Account" entity.
 * An "Account" entity models a single user account. The purpose of this
 * {@link http://docs.angularjs.org/api/ngResource/service/$resource $resource} is to provide the necessary
 * functionality to create users, manage users, authenticate users, etc.
 *
 <pre>
 // Authenticate a user.
 var user = Account.authenticate({ userName: 'rgreen', password: 'abc123' });

 // Retrieve user information.
 user.$userInfo();
 </pre>
 *
 * @requires $resource
 * @requires transcend.security.securityConfig
 * @requires transcend.core.$string
 *
 * See {@link http://docs.angularjs.org/api/ngResource/service/$resource AngularJS's $resource} documentation for
 * details on what methods are natively available to a resource.
 */
/**
     * @ngdoc object
     * @name transcend.security.securityConfig#account
     * @propertyOf transcend.security.securityConfig
     *
     * @description
     * Configuration object for the {@link transcend.security.service:Account Account resource}. This object provides
     * the capability to override the the {@link transcend.security.service:Account Account resource's} default
     * configuration.
     *
     * ## Configuration Properties
     *   * __url__ - a string that that represents the base url to the backend server.
     *
     <pre>
     angular.module('myApp').value('securityConfig', {
       account: {
         url: 'http://localhost/MyWebApiApplication'
       }
     });
     </pre>
     *
     * __Note,__ all methods on a resource can be called on the resource instance by prefixing the method name with a
     * '$'. This is standard {@link transcend.security.service:Account resource} behaviour.
     */
/**
       * @ngdoc method
       * @name transcend.security.service:Account#authenticate
       * @methodOf transcend.security.service:Account
       *
       * @description
       * Authenticates a user account with a set of credentials (user name and password) using an HTTP POST request.
       *
       * @param {Object|String} credentials The credentials to use to authenticate a user. If an object is passed the object
       * must have a 'userName' property and a 'password' property - otherwise if a string is passed than that raw string will
       * get passed to the authentication REST service.
       *
       * @example
       <pre>
       var account = Account.authenticate({ userName: 'user', password: 'password' });
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.security.service:Account#register
       * @methodOf transcend.security.service:Account
       *
       * @description
       * Registers a user account with a set of credentials (user name, password, first name, last name and email)
       * using an HTTP POST request.
       *
       * @param {Object|String} credentials The credentials to use to register a user. If an object is passed the object
       * must have a 'userName', 'password', 'firstName', 'lastName', 'email' property - otherwise if a string is passed
       * than that raw string will get passed to the authentication REST service.
       *
       * @example
       <pre>
       var account = Account.register({ userName: 'user', password: 'password' , confirmPassword: 'password', email: 'myemail@mail.com', firstName: 'first', lastName: 'last'});
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.security.service:Account#signOut
       * @methodOf transcend.security.service:Account
       *
       * @description
       * Signs out an authenticated user. This will invalidate the user's token on the backend server.
       *
       * @example
       <pre>
       var account = Account.authenticate({ userName: 'user', password: 'password' });

       Account.signOut(account);
       // or...
       account.$signOut();
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.security.service:Account#userInfo
       * @methodOf transcend.security.service:Account
       *
       * @description
       * Gets user information for an authenticated user. Such details may include:
       *
       * * Id
       * * User Name
       * * Roles
       * * Email
       * * Contact Info
       *
       * @example
       <pre>
       // Need to have an authenticated user first.
       var account = Account.authenticate({ userName: 'user', password: 'password' });

       Account.userInfo(account);
       // or...
       account.$userInfo();

       // The account object will become the response of the user info call.
       expect(account.email).toEqual('user@tssgis.com);
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.security.service:Account#changePassword
       * @methodOf transcend.security.service:Account
       *
       * @description
       * Changes a password for a signed-in (already authenticated) user.
       *
       * @example
       <pre>
       Account.changePassword({
         oldPassword: 'abc123',
         newPassword: 'def456',
         confirmPassword: 'def456'
       });
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.security.service:Account#forgotPassword
       * @methodOf transcend.security.service:Account
       *
       * @description
       * Send reset password email to an anonymous user with valid username.
       *
       * @example
       <pre>
       Account.forgotPassword({
         redirectTo: 'www.example.com',
         userName: 'user',
         client: 'Team'
       });
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.security.service:Account#resetPassword
       * @methodOf transcend.security.service:Account
       *
       * @description
       * Resets a password for a given username for an anonymous user.
       *
       * @example
       <pre>
       Account.resetPassword({
         resetToken: 'AQAANCabcXYZ%',
         userName: 'user',
         newPassword: 'abc123',
         client: 'Team'
       });
       </pre>
       */
/**
 * @ngdoc directive
 * @name transcend.security.directive:userFinder
 *
 * @description
 * The 'userFinder' directive provides an HTML element that can be used to display and select a user returned from the the Users service. The directive
 * will utilize the bootstrap typeahead directive to query the Users service on each key entry.
 *
 *
 * @restrict EA
 * @element ANY
 *
 * @requires User
 <h2>Roles Using "AND"</h2>
 <pre>
 <div roles="1,2,3">
 The user must have all three roles; 1, 2, and 3 in order to view this div.
 </div>
 </pre>
 <h2>Roles Using "OR"</h2>
 <pre>
 <div roles="1|2|3">
 The user can have role ids of either 1, 2, 3 in order to view this div.
 </div>
 </pre>

 * @example
 <example module="app">
 <file name="index.html">
 <div ng-controller="Ctrl">
 <h1>Roles Example</h1>

 <label>
 User Role IDs:
 <input type="text" ng-model="roleIds" ng-change="setRoles()" placeholder="Example: 1,2,3" />
 </label>

 <div class="row-fluid">
 <div roles="1,2,3" class="span3" style="background-color: #ffc4c4;"><strong>roles="1,2,3"</strong><br/>Must Have Roles 1,2, and 3</div>
 <div roles="5" class="span3" style="background-color: #ffdc9d;"><strong>roles="5"</strong><br/>Must Have Role 5</div>
 <div roles="1|5|10" class="span3" style="background-color: #c5ecc5;"><strong>roles="1|5|10"</strong><br/>Must Have Roles 1,5, or 10</div>
 <div class="span3" style="background-color: #c4c4ff;"><strong>roles=""</strong><br/>No Roles Required (always visible)</div>
 </div>
 </div>
 </file>
 <file name="script.js">
 angular.module('app', ['transcend.security'])

 .value('securityConfig', {
   roleManager: {
     roleIds: [1,2,3,4,5]
   }
 })

 .controller('Ctrl', function($scope, $roleManager, securityConfig) {
    $scope.roleIds = $roleManager.roleIds();
    $scope.setRoles = function () {
      $roleManager.roles($scope.roleIds);
    };
  });
 </file>
 </example>
 */
/**
 * @ngdoc service
 * @name transcend.security.service:User
 *
 * @description
 * The 'User' factory returns configuration parameters for Users
 *
 * @requires $resource
 * @requires transcend.security.securityConfig
 */
/**
 * @ngdoc directive
 * @name transcend.security.directive:userRegistration
 *
 * @description
 * The 'userRegistration' directive provides a template for secure User registration
 *
 * @restrict EA
 * @scope
 *
 * @requires transcend.core.$string
 * @requires transcend.core.$notify
 * @requires transcend.security.$authenticate
 * @requires transcend.security.service:Account
 *
 */
/**
 * @ngdoc service
 * @name transcend.security.$roleManager
 *
 * @description
 * Provides functionality to store and manage user roles. The main purpose of this service is to provide a simple
 * interface to determine if a given role is valid for the stored roles in the role manager. The role manager will let
 * you validate roles based on an "and" or "or" operator.
 *
 <pre>
 $roleManager(credentials);
 </pre>
 *
 * See the {@link transcend.security.directive:roles roles directive} for an interactive example.
 *
 * @requires $log
 * @requires $rootScope
 * @requires transcend.security.securityConfig
 * @requires transcend.core.$array
 *
 * @param {Object|String} credentials The credentials to use to authenticate a user. If an object is passed the object
 * must have a 'userName' property and a 'password' property - otherwise if a string is passed than that raw string will
 * get passed to the authentication REST service.
 */
/**
       * @ngdoc object
       * @name transcend.security.securityConfig#roleManager
       * @propertyOf transcend.security.securityConfig
       *
       * @description
       * Configuration object for the {@link transcend.security.$roleManager $roleManager service}. This object provides
       * the capability to override the the {@link transcend.security.$roleManager $roleManager service's} default
       * configuration.
       *
       * ## Configuration Properties
       *   * __enabled__ - a boolean that determines if the roleManager is enabled or not. If it is not enabled, than all
       *   role checking will be skipped and the "roles" attribute, from the
       *   {@link transcend.security.directive:roles roles directive}, will essentially be ignored.
       *   * __roles__ - an array of pre-defined user Role objects. If you do not have a list of Role objects you can set
       *   just the list of Role Ids.
       *   * __roleIds__ - an array of pre-defined user Role Ids. This property is automatically populated when you set a
       *   list of Role objects. However, if you do not have a list of Role objects you can use this property to use
       *   Role Ids only.
       *
       <pre>
       angular.module('myApp').value('securityConfig', {
       roleManager: {
         roleIds: [1, 2, 3, 4, 5]
       }
     });
       </pre>
       *
       * See the {@link transcend.security.directive:roles roles directive} for a live example of this process.
       */
/**
       * @ngdoc property
       * @name transcend.security.$roleManager#enabled
       * @propertyOf transcend.security.$roleManager
       *
       * @description
       * Flag that states whether the role manager is enabled or not. If it is not enable than all has role calls will
       * always return true. By default this property is set to true.
       * @example
       <pre>
       $roleManager.enabled = false;
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.security.$roleManager#configure
       * @methodOf transcend.security.$roleManager
       *
       * @description
       * Configures the default roles for the role manager. These roles will be what get set after
       * {@link transcend.security.$roleManager#clearRoles} is called.
       * @param {Array|String|Object} configuration The configuration (roles) to set as defaults. An array of roles, a
       * string of roles, or an object with a 'roleIds' property can be passed in.
       * @example
       <pre>
       // Configure the default roles.
       $roleManager.configure([1,2,3]);

       // Set the roles to something else.
       $roleManager.roles([4,5,6]);

       // Clear the roles, and the roles will be reset to the defaults.
       $roleManager.clearRoles();
       expect($roleManager.roles()).toEqual([1,2,3]);
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.security.$roleManager#clearRoles
       * @methodOf transcend.security.$roleManager
       *
       * @description
       * Clears the stored roles and resets the manager to its original state. This function will also fire a
       * 'roles-changed' event.
       * @example
       <pre>
       // Set roles.
       $roleManager.roles([1,2,3]);

       // Clear roles.
       $roleManager.clearRoles();
       expect($roleManager.roles().length).toEqual(0);
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.security.$roleManager#roleIds
       * @methodOf transcend.security.$roleManager
       *
       * @description
       * Gets and/or sets role ids. If a value is passed in it will set role ids to the value, otherwise it will only
       * return the stored role ids.
       * @param {Array|String} roleIds An array or comma separated string or role ids to set as the stored role ids.
       * @example
       <pre>
       // Set role ids.
       $roleManager.roleIds([1,2,3]);

       // Get role ids.
       expect($roleManager.roleIds()).toEqual([1,2,3]);
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.security.$roleManager#roles
       * @methodOf transcend.security.$roleManager
       *
       * @description
       * Gets and/or sets roles. If a value is passed in it will set role ids to the value, otherwise it will only
       * return the stored roles. This function will also update the role ids based on the passed in roles.
       * @param {Array|String=} roles An array or comma separated string or roles to set as the stored roles.
       * @example
       <pre>
       // Set role ids.
       $roleManager.roles([{id: 1},{id: 2},{id: 3}]);

       // Get roles.
       expect($roleManager.roles()).toEqual([{id: 1},{id: 2},{id: 3}]);

       // Setting the roles also updates the role ids.
       expect($roleManager.roleIds()).toEqual([1,2,3]);
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.security.$roleManager#hasRole
       * @methodOf transcend.security.$roleManager
       *
       * @description
       * Determines if the passed in role has permissions based on the stored roles.
       *
       * Note, if the {@link transcend.security.$roleManager#enabled enabled} property is set to false than this method
       * will always return true.
       *
       * See the {@link transcend.security.directive:roles roles directive} for an interactive example.
       * @param {String|Number} role A role or role id to validate against the stored roles.
       * @example
       <pre>
       // Set role ids.
       $roleManager.roles([1,2,3]);

       expect($roleManager.hasRole(1)).toBeTruthy();
       expect($roleManager.hasRole(9)).toBeFalsy();
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.security.$roleManager#hasRoles
       * @methodOf transcend.security.$roleManager
       *
       * @description
       * Determines if the passed in set of roles has permissions based on the stored roles. This is the same
       * functionality as the {link transcend.security.$roleManager#hasRole "hasRole" method} except it allows you to
       * pass multiple roles to check against, as well as an "all inclusive" flag to state whether to use an "and" or "or"
       * operator when checking roles.
       *
       * If you pass a string you can specify the "all inclusive" flag to false by separating the role values with a
       * pipe (|).
       *
       * Note, if the {@link transcend.security.$roleManager#enabled enabled} property is set to false than this method
       * will always return true.
       *
       * See the {@link transcend.security.directive:roles roles directive} for an interactive example.
       * @param {String|Number|Array} roles A set of roles to validate against the stored roles.
       * @param {Boolean} allInclusive Flag to determine if to use the "and" or "or" operator. If true is passed in than
       * the "and" operator will be used. If no value is passed in than it will look for a pipe (|) in the passed in roles
       * and will use "or" if it finds one - otherwise it will default to "and".
       <pre>
       // Will return true if role 1 or 2 or 3 are in the stored roles.
       $roleManager.hasRoles('1|2|3'));

       // Will return true if role 1 and 2 and 3 are all in the stored roles.
       $roleManager.hasRoles('1,2,3'));
       </pre>
       *
       * @example
       <pre>
       // Set role ids.
       $roleManager.roles(['Admin','User', 'Guest']);

       // Will use the "and" operator:
       expect($roleManager.hasRoles(['Admin', 'User'])).toBeTruthy();
       expect($roleManager.hasRoles('Admin, User')).toBeTruthy();
       expect($roleManager.hasRoles('Admin User Guest')).toBeTruthy();

       // Will use the "or" operator
       expect($roleManager.hasRoles('Admin|SuperUser')).toBeTruthy();
       </pre>
       */
/**
 * @ngdoc directive
 * @name transcend.security.directive:roles
 *
 * @description
 * The 'roles' directive provides a simple way to define an HTML element as having a particular role id. The directive
 * will then automatically hide/show the HTML depending on whether the signed in user has privileges to view that role.
 *
 * Note: This hide/showing of content depending on roles prevents the user from seeing the content but does not
 * provide a secure application by itself - the REST services that these components interact with must have their own
 * security implementation. In other words, since the client side code is just JavaScript anyone can manipulate the code
 * or DOM to show/hide what they would like - so the true security must happen on the REST services.
 *
 * @restrict A
 * @element ANY
 *
 * @requires $parse
 * @requires transcend.security.securityConfig
 * @requires transcend.security.$roleManager
 *
 * @param {Array} roles The roles/role ids that the user must have in order to view the HTML. The roles can be passed in
 * as comma-delimited or pipe-delimited. When passing a comma-delimited list the roles logic will use an "AND" operator,
 * while the pipe-delimited list will use an "OR" operator.
 <h3>Roles Using "AND"</h3>
 <pre>
 <div roles="1,2,3">
 The user must have all three roles; 1, 2, and 3 in order to view this div.
 </div>
 </pre>
 <h3>Roles Using "OR"</h3>
 <pre>
 <div roles="1|2|3">
 The user can have role ids of either 1, 2, 3 in order to view this div.
 </div>
 </pre>
 *
 * @param {expression} rolesPass An expression to evaluate when roles pass for the given "roles".
 <pre>
 <div roles="1,2,3" roles-pass="pass"></div>

 <div ng-if="pass">
 Congratulations! You have access.
 </div>
 </pre>
 *
 * @param {expression} rolesFail An expression to evaluate when roles fail for the given "roles".
 <pre>
 <div roles="1,2,3" roles-fail="fail"></div>

 <div ng-if="fail">
 Sorry! You do not have access.
 </div>
 </pre>
 *
 * @example
 <example module="app">
 <file name="index.html">
 <div ng-controller="Ctrl">
 <h1>Roles Example</h1>

 <label>
 User Role IDs:
 <input type="text" ng-model="roleIds" ng-change="setRoles()" placeholder="Example: 1,2,3" />
 </label>

 <div class="row-fluid">
 <div roles="1,2,3" class="span3" style="background-color: #ffc4c4;"><strong>roles="1,2,3"</strong><br/>Must Have Roles 1,2, and 3</div>
 <div roles="5" class="span3" style="background-color: #ffdc9d;"><strong>roles="5"</strong><br/>Must Have Role 5</div>
 <div roles="1|5|10" class="span3" style="background-color: #c5ecc5;"><strong>roles="1|5|10"</strong><br/>Must Have Roles 1,5, or 10</div>
 <div class="span3" style="background-color: #c4c4ff;"><strong>roles=""</strong><br/>No Roles Required (always visible)</div>
 </div>
 </div>
 </file>
 <file name="script.js">
 angular.module('app', ['transcend.security'])

 .value('securityConfig', {
   roleManager: {
     roleIds: [1,2,3,4,5]
   }
 })

 .controller('Ctrl', function($scope, $roleManager, securityConfig) {
    $scope.roleIds = $roleManager.roleIds();
    $scope.setRoles = function () {
      $roleManager.roles($scope.roleIds);
    };
  });
 </file>
 </example>
 */
/**
       * @ngdoc object
       * @name transcend.security.securityConfig#roles
       * @propertyOf transcend.security.securityConfig
       *
       * @description
       * Configuration object for the {@link transcend.security.directive:roles roles directive}. This object provides
       * the capability to override the the {@link transcend.security.directive:roles roles directive} default
       * configuration.
       *
       * ## Configuration Properties
       *   * __bypass__ - a flag to bypass the roles functionality (disable hiding content based on roles). The
       *   default value is false - meaning it is enabled.
       *
       <pre>
       angular.module('myApp').value('securityConfig', {
         roles: {
           bypass: true
         }
       });
       </pre>
       */
